# README #

### For this code to work you will need to install: ###

* git
* Node.JS 0.10.x or higher
* NPM package manager

### How do I get set up? ###

* Clone this repository
* Run [npm install] where the package.json file is located
* Edit and change the settings.js according to your configuration needs: 
**cp settings.js.example settings.js**
* Copy .htaccess.example and edit all caps variables:
**cp .htaccess.example .htaccess**
* Run main.js as PM2 application (verify that server is listening on specified port):
**pm2 start main.js**
* Configure a Bitbucket Webhook POST hook in your Repository settings and add as follows: ** http://yourserver.com/deploy/