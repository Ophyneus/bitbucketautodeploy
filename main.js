var express = require('express'),
    http = require('http'),
    bodyParser = require('body-parser'),
    settings = require('./settings'),
    _ = require("underscore"),
    nodemailer = require('nodemailer'),
    app = express();

    app.use( bodyParser.json() );       // to support JSON-encoded bodies
    app.use( bodyParser.urlencoded({ extended: true }) ); // to support URL-encoded bodies

    var script;

    var server = app.listen(settings.port, function() {
        console.log('Listening on port %d', server.address().port);
    });

    let transporter = nodemailer.createTransport({
        sendmail: true,
        newline: 'unix'
    });

    // handle the post messages
    app.post(settings.subdir, function (req, res) {

        var data = req.body;
        var branch = data.push.changes[0].new.name;
        var repositoryURL = data.repository.full_name;

        console.log("Validating if commit is part of monitored scope");

        var repository = _.find(settings.targets, function(record){
           script = record.script;
           return record.url == repositoryURL;
        });

         console.log("Repository target found: " + repositoryURL);
         console.log('Detecting branch: ' + branch);

         if (branch == settings.req_branch) {
             console.log("Trigger update!");
             var spawn = require('child_process').spawn,
             deploy = spawn(script);

             deploy.stdout.on('data', function (data) {
                 console.log(''+data);
                 transporter.sendMail({
                      from: settings.from_email,
                      to: settings.to_email,
                      subject: settings.subject_email,
                      text: data
                  }, (err, info) => {
                      console.log(info.envelope);
                      console.log(info.messageId);
                  });
             });

             deploy.on('close', function (code) {
                 console.log('Child process exited with code ' + code);
             });
        } else console.log("Push registered but branch was not master. Found: " + branch);

        res.status(200).json({message: 'Bitbucket Hook received!'});
    });
